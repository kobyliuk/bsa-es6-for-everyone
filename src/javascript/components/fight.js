import { controls } from '../../constants/controls';
import Fighter from './../model/fighter';
import { changeHealthbarWidth } from './fightersView';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let winnerPlayer = '';
    const activeButton = new Set();
    const players = createFighters(firstFighter, secondFighter);
    const onKeyDown = (e) => {
      handleKeyDown(e, activeButton, players);
      winnerPlayer= endGame(players);
      if(winnerPlayer) {
        (winnerPlayer === 1) && resolve(firstFighter);
        (winnerPlayer === 2) && resolve(secondFighter);
        document.removeEventListener('keydown', onKeyDown);
        document.removeEventListener('keyup', onKeyUp);
      }
    };
    const onKeyUp = (e) => handleKeyUp(e, activeButton, players);
    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage < 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  const criticalHit = Math.random() + 1;
  return fighter.attack * criticalHit;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

function getUltraAttackPower(fighter) {
  return fighter.attack * 2;
}

function createFighters(firstFighter, secondFighter) {
  return {
    'firstFighter': new Fighter(1, firstFighter),
    'secondFighter': new Fighter(2, secondFighter)
  };
}

function handleKeyDown(e, btnSet, {firstFighter, secondFighter}) {
  if(btnSet.has(e.code)) return;
  btnSet.add(e.code);
  if(e.code === controls.PlayerOneBlock) {
    return firstFighter.canBlock = true;
  }
  if(e.code === controls.PlayerTwoBlock) {
    return secondFighter.canBlock = true;
  }
  fightAction(btnSet, {firstFighter, secondFighter});
}

function handleKeyUp(e, btnSet, {firstFighter, secondFighter}) {
  btnSet.delete(e.code);
  switch (e.code) {
    case controls.PlayerOneAttack:
      return firstFighter.canAttack = true;
    case controls.PlayerTwoAttack:
      return secondFighter.canAttack = true;
  }
}

function fightAction(btnSet, {firstFighter, secondFighter}) {
  switch(true) {
    case doUltraAttack(btnSet, controls.PlayerOneCriticalHitCombination, controls.PlayerOneBlock, firstFighter.canUltraAttack):
      return effectUltraAttack(firstFighter, secondFighter);
    case doUltraAttack(btnSet, controls.PlayerTwoCriticalHitCombination, controls.PlayerTwoBlock, secondFighter.canUltraAttack):
      return effectUltraAttack(secondFighter, firstFighter);
    case doAttack(btnSet, controls.PlayerOneAttack, controls.PlayerOneBlock, firstFighter.canAttack):
      return effectAttack(firstFighter, secondFighter, btnSet, controls.PlayerTwoBlock);
    case doAttack(btnSet, controls.PlayerTwoAttack, controls.PlayerTwoBlock, secondFighter.canAttack):
      return effectAttack(secondFighter, firstFighter, btnSet, controls.PlayerOneBlock);
    default:
      return;
  }
}

function doAttack(btnSet, controlAttack, controlBlock, canAttack) {
  return (btnSet.has(controlAttack) && !btnSet.has(controlBlock) && canAttack);
}


function effectAttack(attacker, defender, btnSet, controlDefense) {
  attacker.canAttack = false;
  const damage = btnSet.has(controlDefense) ? getDamage(attacker, defender) : getHitPower(attacker);
  defender.decreaseHealth(damage);
  const positionDefender = (defender.num === 1) ? 'left' : 'right';
  changeHealthbarWidth(defender, positionDefender);
}

function doUltraAttack(btnSet, controlAttack, controlBlock, canUltraAttack) {
  return (checkUltraAttack(btnSet, controlAttack) && !btnSet.has(controlBlock) && canUltraAttack);
}

function checkUltraAttack(btnSet, control) {
  if(btnSet.size < control) {
    return false;
  }
  return controlValuesInSet(btnSet, control);
}

function controlValuesInSet(set, control) {
  const len = control.length;
  let result = true;
  for(let i = 0; i < len; i++) {
    if(!set.has(control[i])) {
      result = false;
      break;
    }
  }
  return result;
}

function delayUltraAttack(player, position) {
  player.canUltraAttack = false;
  setTimeout(() => {
    player.canUltraAttack = true;
  }, 10000);
}

function effectUltraAttack(attacker, defender) {
  const positionAttacker = (attacker.num === 1) ? 'left' : 'right';
  const positionDefender = (defender.num === 1) ? 'left' : 'right';
  defender.decreaseHealth(getUltraAttackPower(attacker));
  changeHealthbarWidth(defender, positionDefender);
  delayUltraAttack(attacker, positionAttacker);
}


function endGame({firstFighter, secondFighter}) {
  switch (true) {
    case (firstFighter.health === 0):
      return 2;
    case (secondFighter.health === 0):
      return 1;
    default:
      return '';
  }
}
