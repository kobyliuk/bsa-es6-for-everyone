import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
export function showWinnerModal(fighter) {
  const imgFighter = createFighterImage(fighter);
  showModal({
    title:  `${fighter.name} WINNER!`,
    bodyElement: imgFighter,
    onClose: () => { location.reload(); }
  });
}
