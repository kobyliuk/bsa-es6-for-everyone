class Fighter {

  constructor(num, {health, defense, attack }) {
    this.num = num;
    this.health = health;
    this.defense = defense;
    this.attack = attack;
    this.canAttack = true;
    this.canBlock = true;
    this.canUltraAttack = true;
  }

  decreaseHealth = (value) => {
    this.health = ((this.health - value) > 0) ? (this.health - value) : 0;
  };
}


export default Fighter;
